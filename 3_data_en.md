This challenge is not a supervised learning problem (classification or regression) but a **Reinforcement Learning** problem:

* There is no oracle;
* the algorithm has to **learn to behave**;
* it solve problems that involve **making a sequence of (optimal) decisions** (control).

` `

![MDP](https://gitlab.com/ens-data-challenge/challenge-description/-/raw/master/figs/mdp.png?inline=false)

In this framework, we have a system (or *environment*) which is in a given *state*; the *agent* (i.e. the adaptive algorithm in charge to control our system) acts on this environment with *actions*.
*Agent* *actions* modify the *state* of the *environment*, and these *actions* generates a *reward*.
The goal is to maximize the sum of these *rewards* over time, knowing that there is a stochastic process which will also have an influence on the evolution of the *environment*.

In this challenge, you have to train an AI (your *agent*) to drive a simplified numerical model of the ACCENTA low-carbon thermal system (the *environment*) so as to maximize its efficiency and minimize its energy bill.

![MDP](https://gitlab.com/ens-data-challenge/challenge-description/-/raw/master/figs/mdp_accenta.png?inline=false)

At each time step, your agent will have a certain amount of information at his disposal (which define the ***state space***) to make his decisions:

* the local weather conditions of the building (this information is important because the amount of heat to be produced for the building depends strongly on the outside temperature);
* the weather forecast for the coming hours;
* the amount of heat that the building needs at the moment t to respect the thermal comfort of its users;
* the current thermal stock level in the building;
* temporal information (i.e. the current date and time).

` `

Your agent will have several levers at his disposal to control the thermal system (which define the ***actions*** space):

* the amount of energy produced with each of the thermal system's available equipment knowing that each piece of equipment has a variable efficiency depending on current conditions (weather and current thermal need) and that the choice of distribution of this production will have an impact on the long term, in other words, the decisions made at time t impact the efficiency of future decisions ;
* the amount of energy to be stored or de-stored.

` `

![MDP](https://gitlab.com/ens-data-challenge/challenge-description/-/raw/master/figs/mpc_small.gif?inline=false)
