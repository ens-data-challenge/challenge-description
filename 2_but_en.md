## Goal

**Reducing greenhouse gas emissions** of heating and cooling systems of large buildings

## Means

**Optimizing** the control of the thermal systems of these buildings

**Significant CO2 savings** are already achieved by Accenta:

* by introducing  heat storage systems that allow heat to be **produced, stored and consumed at the best times**;
* by optimizing management of heat stocks using **predictive algorithms, adaptive**  to the specific conditions of each building..


## Challenge

Explore new ways to improve these algorithms based on **reinforcement learning methods** 