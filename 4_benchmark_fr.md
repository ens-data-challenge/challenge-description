## Préambule

Ce challenge n’étant pas un challenge d’apprentissage supervisé, le processus d’écriture et d’évaluation du modèle est un peu différent de celui des autres challenges.

L’environnement sur lequel entraîner votre agent sera installé via la commande `pip` comme n’importe quelle bibliothèque Python :

```shell
pip install rlenv
```

` `

Vous serez donc amené à écrire et entraîner votre agent sur votre machine et une fois que vous serez satisfait du résultat, vous pourrez le publier pour évaluation sur la plateforme ENS via un référentiel Git que vous aurez préalablement créé sur GitHub ou GitLab.

Le choix d’implémentation de l’agent est très libre tant au niveau technique qu’algorithmique.
La seule contrainte est d’implémenter la fonction [rlagent.agent.load_agent()](https://ens-data-challenge.gitlab.io/accenta-rl-agent-example/api_agent.html#rlagent.agent.load_agent) dans le code de votre modèle pour que la plateforme d’évaluation puisse charger votre agent entraîné.

Tout cela est expliqué sur la page suivante : [https://ens-data-challenge.gitlab.io/accenta-rl-agent-example/api_agent.html#rlagent.agent.load_agent](https://ens-data-challenge.gitlab.io/accenta-rl-agent-example/api_agent.html#rlagent.agent.load_agent)

Un exemple d’agent pré entraîné est mis à disposition sur le référentiel Git suivant : [https://gitlab.com/ens-data-challenge/accenta-rl-agent-example](https://gitlab.com/ens-data-challenge/accenta-rl-agent-example)

La suite de ce document détaille la procédure technique à suivre pour installer l'environnement, entraîner l'agent et l'évaluer sur la plateforme *Challenge Data*.

## Installation de l'environnement de programmation sur Linux, Mac OS X ou WSL

Nous supposons ici que l'environnement de programmation Python est déjà installé sur votre ordinateur.

Voici les étapes à suivre pour préparer votre environnement pour ce challenge :

* Créez un compte sur [https://gitlab.com](https://gitlab.com/) si vous n'est possédez pas déjà un

` `

![MDP](https://gitlab.com/ens-data-challenge/challenge-description/-/raw/master/figs/install_1.png?inline=false)

` `

* Créez un *Fork* de l'agent fourni en exemple sur GitLab [https://gitlab.com/ens-data-challenge/accenta-rl-agent-example](https://gitlab.com/ens-data-challenge/accenta-rl-agent-example)

` `

![MDP](https://gitlab.com/ens-data-challenge/challenge-description/-/raw/master/figs/install_2.png?inline=false)

` `

* Le *Fork* crée une copie du projet [accenta-rl-agent-example](https://gitlab.com/ens-data-challenge/accenta-rl-agent-example) dans votre espace personnel GitLab. Vous pouvez maintenant *Cloner* cette copie :

` `

![MDP](https://gitlab.com/ens-data-challenge/challenge-description/-/raw/master/figs/install_3.png?inline=false)

` `

```shell
git clone git@gitlab.com:VOTRE_COMPTE/accenta-rl-agent-example.git
```

` `

Pour plus d'aide, sur l'utilisation de git et gitlab, veuillez consulter le lien suivant : [make_your_first_git_commit.html](https://docs.gitlab.com/ee/tutorials/make_your_first_git_commit.html)

` `

* Installez l'environnement sur votre ordinateur (c.f. [la procédure d'installation](https://gitlab.com/ens-data-challenge/accenta-rl-agent-example/-/blob/master/README.rst#installation))

` `

```shell
cd accenta-rl-agent-example
python3 -m venv env
source env/bin/activate
python3 -m pip install --upgrade pip
python3 -m pip install -r requirements.txt
python3 setup.py develop
```

` `

* Testez la bonne installation de l'environnement

```shell
python3 examples/plot_load_and_run_agent.py
```

` `

![MDP](https://gitlab.com/ens-data-challenge/challenge-description/-/raw/master/figs/res.png?inline=false)


D'autres scripts d'exemples sont disponibles [ici](https://ens-data-challenge.gitlab.io/accenta-rl-agent-example/gallery/).


## Définition et entraînement de l'agent

L'environnement est installé, vous pouvez alors commencer à travailler sur la partie algorithmique.

Un script d'exemple montrant la définition et l'entraînement votre agent est disponible dans [examples/print_train_and_save_agent.py](https://gitlab.com/ens-data-challenge/accenta-rl-agent-example/-/blob/master/examples/print_train_and_save_agent.py). L'exemple fourni est volontairement minimaliste et s'appui sur une implémentation toute faite de [PPO](https://arxiv.org/abs/1707.06347) dans [Stable-Baselines3](https://stable-baselines3.readthedocs.io/en/master/) ; à vous d'être créatif pour faire mieux que l'exemple fourni !

Les dépendances dont à besoin votre agent doivent être placées dans le fichier [requirements.txt](https://gitlab.com/ens-data-challenge/accenta-rl-agent-example/-/blob/master/requirements.txt) à la racine du projet (pytorch et stablebaseline3 sont pré-renseignés).

## Évaluation de l'agent sur la plateforme Challenge Data

Pour évaluer votre agent entraîné, vous devez publier sont code et les paramètres de son modèle sur gitlab.com (via les commandes ["git add", "git commit" et "git push"](https://docs.gitlab.com/ee/tutorials/make_your_first_git_commit.html#commit-and-push-your-changes)).
L'agent doit pouvoir être chargé depuis n'importe quel machine via la fonction [rlagent.agent.load_agent()](https://gitlab.com/ens-data-challenge/accenta-rl-agent-example/-/blob/master/rlagent/agent.py#L6).

Allez ensuite sur la [page "Soumissions" de la plateforme Challenge Data](https://challengedata.ens.fr/participants/challenges/82/submissions) pour évaluer votre agent. N'oubliez pas de renseigner l'url de votre projet GitLab.
