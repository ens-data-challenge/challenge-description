Ce challenge n'est pas un problème d'apprentissage supervisé (classification ou régression) mais un problème d'**apprentissage par renforcement** :

* il n'y a pas d'*oracle* ;
* l'algorithme doit **apprendre à se comporter** ;
* il résout des problèmes qui impliquent de prendre une **séquence de décisions optimisée** (contrôle).

` `

![MDP](https://gitlab.com/ens-data-challenge/challenge-description/-/raw/master/figs/mdp.png?inline=false)

Le problème traité est un processus de **contrôle stochastique discret** (appelé *Processus de Décision Markovien* ou PDM). Dans ce cadre, nous avons un système (ou ***environnement***) qui à l'instant $`t`$ (discret) est dans un ***état*** $`\boldsymbol{s}_t \in \mathcal{S}`$ donné. L'***agent*** (c'est-à-dire l'algorithme adaptatif chargé de contrôler notre système) agit sur cet *environnement* par une ***action*** choisi $`\boldsymbol{a}_t \in \mathcal{A}`$. Ce choix est défini par la ***politique*** $`\pi`$ de l'agent. Cette politique peut être déterministe ($`\pi : \mathcal{S} \rightarrow \mathcal{A}`$) ou stochastique ($`\pi : \mathcal{S} \times \mathcal{A} \rightarrow [0;1]`$).
L'*action* de l'agent modifie l'*état* de l'*environnement*. Le nouvel état est noté $`\boldsymbol{s}_{t+1}`$. L'execution de l'*action* génèrent une ***récompense*** $`r_t : \mathcal{S} \times \mathcal{A} \times \mathcal{S} \rightarrow \mathbb{R}`$ qui dépend de l'ancien état $`\boldsymbol{s}_{t}`$ de l'action $`\boldsymbol{a}_{t}`$ et du nouvel état $`\boldsymbol{s}_{t+1}`$.
Le but est de trouver la politique $`\pi^*`$ de l'agent qui maximise l'espérance la somme de ces *récompenses* au cours du temps $`\sum_{k=0}^T r_k`$ (où $`T`$ est le pas de temps final de la simulation), sachant que les changements d'états sont également perturbés par un processus stochastique : la distribution $`P(\boldsymbol{s}_{t+1} | \boldsymbol{s}_t, \boldsymbol{a}_t)`$ définie la probabilité que l'environnement se trouve dans le nouvel état $`\boldsymbol{s}_{t+1}`$ quand l'agent execute l'action $`\boldsymbol{a}_{t}`$ depuis l'état courant $`\boldsymbol{s}_{t}`$.

Dans ce challenge, vous devez entraîner une IA (votre ***agent***) à piloter un modèle numérique simplifié du système thermique bas carbone ACCENTA (l'***environnement***) de sorte à maximiser l'efficacité et à minimiser la facture énergétique de ce système.

![MDP](https://gitlab.com/ens-data-challenge/challenge-description/-/raw/master/figs/mdp_accenta.png?inline=false)

À chaque pas de temps, pour prendre ses décisions votre agent a à sa disposition un certain nombre d'informations (qui définissent l'*espace des états* $`\mathcal{S}`$) :

* les conditions météos locales au bâtiment (cette information est importante car la quantité de chaleur à produire pour le bâtiment dépend fortement de la température extérieure) ;
* les prévisions météo pour les heures à venir ;
* la quantité de chaleur dont le bâtiment a besoin à l’instant t   pour respecter le confort thermique de ses usagers ;
* le niveau de stock thermique actuel dans le bâtiment ;
* une information temporelle (donc la date et l’heure courante).

` `

Votre agent aura plusieurs leviers à sa disposition pour piloter le système thermique (qui définissent l'espace des *actions* $`\mathcal{A}`$) :

* la quantité d’énergie produite avec chacun des équipements du système thermique disponibles sachant que chaque équipement a une efficacité variable en fonction des conditions courantes (météo et besoin thermique actuel) et que le choix de répartition de cette production aura un impact sur le long terme, autrement dit, les décisions prises au temps t impactent l’efficacité des décisions futures ;
* la quantité d’énergie à stocker ou déstocker.

` `

![MDP](https://gitlab.com/ens-data-challenge/challenge-description/-/raw/master/figs/mpc_small.gif?inline=false)

` `

La complexité de ce type de problème (*Temporal credit assignment problem*) réside dans le fait que :

* les décisions sont corrélées (les choix faits à l'instant $`t`$ ont un impacts sur les gains espérés des prochaines actions) ;
* la réponse de l'environnement aux actions est incertaine ;
* les gains sont souvent décalés dans le temps (les effets d'une bonne ou d'une mauvaise action ne sont visibles que longtemps après) ;
* les corrélations état, action, gain sont difficiles à percevoir (signaux faibles).