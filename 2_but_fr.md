Le sujet de ce challenge est la **réduction des émissions de gaz à effet de serre** causées par le chauffage et la climatisation des bâtiments.
L'objectif est de minimiser ces émissions en optimisant le pilotage des systèmes thermiques des bâtiments collectifs.

Des **économies drastiques de CO2** sont d'ores et déjà réalisées par Accenta :

* en dotant les bâtiments de systèmes de stockage de la chaleur qui permettent de **produire la chaleur, de la conserver et de la consommer aux meilleurs moments** ;
* en **optimisant** la gestion de ces stocks de chaleur à l'aide d'**algorithmes de pilotage prédictifs** (qui anticipent le futur) et **adaptatifs** (qui apprennent de leurs erreurs et s'adaptent aux conditions spécifiques de chaque bâtiment).

` `

Le challenge proposé consiste à explorer de nouvelles pistes pour améliorer ces algorithmes en s’appuyant sur des **méthodes d’apprentissage par renforcement**.